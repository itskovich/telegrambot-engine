module bitbucket.org/itskovich/telegrambot-engine

go 1.17

require github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible

require (
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
