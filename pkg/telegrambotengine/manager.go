package telegrambotengine

import (
	"fmt"
	"github.com/patrickmn/go-cache"
	"log"
	"time"
)
import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"

type IBotEngine interface {
	Start() error
	DialogCount() int
	Bot() *tgbotapi.BotAPI
}

type IDialogFactory interface {
	Create(bot *tgbotapi.BotAPI, chat *tgbotapi.Chat) IDialog
}

type BotEngineImpl struct {
	IBotEngine

	dialogCache   *cache.Cache
	bot           *tgbotapi.BotAPI
	DialogFactory IDialogFactory
	Token         string
}

func (c *BotEngineImpl) Bot() *tgbotapi.BotAPI {
	return c.bot
}

func (c *BotEngineImpl) DialogCount() int {
	if c.dialogCache == nil {
		return 0
	}
	return c.dialogCache.ItemCount()
}

func (c *BotEngineImpl) Start() error {

	c.dialogCache = cache.New(cache.NoExpiration, cache.NoExpiration)
	go c.destroyAllDialogs()

	bot, err := tgbotapi.NewBotAPI(c.Token)
	if err != nil {
		return err
	}
	c.bot = bot

	bot.Debug = true

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	for update := range updates {

		if update.Message == nil && update.CallbackQuery == nil { // ignore any non-Message Updates
			continue
		}

		if update.Message != nil {
			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
			c.getDialog(update.Message.Chat, update.Message, true).OnMessage(update.Message)
		} else if update.CallbackQuery != nil {
			c.getDialog(update.CallbackQuery.Message.Chat, update.Message, false).OnCallbackQuery(update.CallbackQuery)
		}
	}

	return nil
}

func (c *BotEngineImpl) getDialog(chatId *tgbotapi.Chat, firstMsg *tgbotapi.Message, startLifecycle bool) IDialog {

	key := fmt.Sprintf("%v", chatId.ID)

	dialogI, exists := c.dialogCache.Get(key)
	if exists {
		return dialogI.(IDialog)
	}

	dialog := c.DialogFactory.Create(c.bot, chatId)
	c.dialogCache.Set(key, dialog, cache.NoExpiration)

	// lifecycle
	//if startLifecycle {
		go func() {
			dialog.OnStart(firstMsg)
			dialog.OnStop()
			dialog.Destroy()
			c.dialogCache.Delete(key)
		}()
	//}

	return dialog
}

func (c *BotEngineImpl) destroyAllDialogs() {
	for {
		time.Sleep(time.Hour)
		for _, v := range c.dialogCache.Items() {
			d := v.Object.(IDialog)
			if d.Expired() {
				println(fmt.Sprintf("Destroy dialog with ID=%v, expired", d.Chat().ID))
				d.Destroy()
			}
		}
	}
}
