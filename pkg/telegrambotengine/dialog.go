package telegrambotengine

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"time"
)

type IDialog interface {
	Print(msg string) (tgbotapi.Message, error)
	Input(prompt string) tgbotapi.Message
	OnStart(firstMsg *tgbotapi.Message)
	OnStop()
	OnMessage(message *tgbotapi.Message)
	Chat() *tgbotapi.Chat
	PrintCustom(msg tgbotapi.MessageConfig) (tgbotapi.Message, error)
	Cancel()
	Cancelled() bool
	Destroy()
	Expired() bool
	OnCallbackQuery(q *tgbotapi.CallbackQuery)
	Bot() *tgbotapi.BotAPI
}

type BaseDialogImpl struct {
	IDialog

	lastMsg      *tgbotapi.Message
	bot          *tgbotapi.BotAPI
	chat         *tgbotapi.Chat
	inputBlocker chan tgbotapi.Message
	cancelled    bool
}

func (c *BaseDialogImpl) Expired() bool {
	return c.lastMsg != nil && time.Since(c.lastMsg.Time()).Hours() > 240 // 10 суток тишины - диалог уничтожаем
}

func (c *BaseDialogImpl) Cancelled() bool {
	return c.cancelled
}

func (c *BaseDialogImpl) OnCallbackQuery(q *tgbotapi.CallbackQuery) {
	if !c.cancelled {
		if c.inputBlocker != nil {
			c.inputBlocker <- *q.Message
		}
	}
}

func (c *BaseDialogImpl) Cancel() {
	c.cancelled = true
}

func (c *BaseDialogImpl) Destroy() {
	//c.Cancel()
	//if c.inputBlocker != nil {
	//	//c.inputBlocker <- tgbotapi.Message{Text: "cancelled"}
	//	close(c.inputBlocker)
	//	c.inputBlocker = nil
	//}
}

func (c *BaseDialogImpl) Chat() *tgbotapi.Chat {
	return c.chat
}

func NewBaseDialog(bot *tgbotapi.BotAPI, chat *tgbotapi.Chat) BaseDialogImpl {
	return BaseDialogImpl{
		bot:  bot,
		chat: chat,
	}
}

func (c *BaseDialogImpl) OnMessage(message *tgbotapi.Message) {
	c.lastMsg = message

	if c.inputBlocker != nil {
		c.inputBlocker <- *message
	} else {
		if !c.cancelled {
			c.inputBlocker = make(chan tgbotapi.Message)
		}
	}
}

func (c *BaseDialogImpl) OnStart(firstMsg *tgbotapi.Message) {
}

func (c *BaseDialogImpl) OnStop() {
}

func (c *BaseDialogImpl) PrintCustom(msg tgbotapi.MessageConfig) (tgbotapi.Message, error) {
	//msg.ChatID = c.chat.ID
	return c.bot.Send(msg)
}

func (c *BaseDialogImpl) Bot() *tgbotapi.BotAPI {
	return c.bot
}

func (c *BaseDialogImpl) Print(msg string) (tgbotapi.Message, error) {
	m := tgbotapi.NewMessage(c.chat.ID, msg)
	return c.bot.Send(m)
}

func (c *BaseDialogImpl) Input(prompt string) tgbotapi.Message {
	if !c.cancelled {
		c.Print(prompt)
		if c.inputBlocker != nil {
			select {
			case answer := <-c.inputBlocker:
				return answer
			}
		} else {
			c.inputBlocker = make(chan tgbotapi.Message)
		}
	}
	return tgbotapi.Message{}
}

func (c *BaseDialogImpl) InputCustom(prompt tgbotapi.MessageConfig) tgbotapi.Message {
	if !c.cancelled {
		c.PrintCustom(prompt)
		if c.inputBlocker != nil {
			select {
			case answer := <-c.inputBlocker:
				return answer
			}
		}
	}
	return tgbotapi.Message{}
}
